// Try Hokkaido Univ.& Hitachi 1st New-concept Computing Contest 2017
// author: Leonardone @ NEETSDKASU

fn main() {

    use std::time::{Duration, SystemTime};
    let loop_out_time = SystemTime::now() + Duration::from_millis(9800);
    
    
    let mut stdin = String::new();
    {
        use std::io::Read;
        std::io::stdin().read_to_string(&mut stdin).expect("failed to read string");    
    }
    let mut stdin = stdin.split_whitespace();
    
    macro_rules! get {
        () => ( stdin.next().unwrap().parse().unwrap() );
        ($t:ty) => ( stdin.next().unwrap().parse::<$t>().unwrap() );
    }

    let v = get!(usize);
    let e = get!(u32);
    
    let mut table = vec![vec![0; v+1]; v+1];
    let mut edge_total = vec![0; v+1];
    for _ in 0..e {
        let a = get!(usize);
        let b = get!(usize);
        let w = get!(i64);
        table[a][b] = w;
        table[b][a] = w;
        edge_total[a] += w;
        edge_total[b] += w;
    }
    let table = table;
    let edge_total = edge_total;
    
    let vemb = get!(usize);
    let s = (vemb as f64).sqrt() as usize;
    
    let rsz = vemb;
    let mut res = vec![0; rsz];
    let mut max_score = 0;
    for i in 0..v {
        res[i] = i + 1;
    }
    
    let mut mods = vec![0; rsz+s+2];
    for i in 0..rsz+s+2 {
        mods[i] = i % s;
    }
    let mods = mods;
    
    macro_rules! calc {
        ($r:ident; $p:expr) => ( calc!($r; $p; $r[$p]) );
        ($r:ident; $p:expr; $v:expr) => {
            {
                let mut tmp = 0;
                let md = mods[$p+1];
                if md != 1 {
                    if $p > 0 {
                        tmp += table[$v][$r[$p-1]];
                        if $p >= s+1 {
                            tmp += table[$v][$r[$p-(s+1)]];
                        }
                    }
                    if $p+(s-1) < rsz {
                        tmp += table[$v][$r[$p+(s-1)]];
                    }
                }
                if md != 0 {
                    if $p+1 < rsz {
                        tmp += table[$v][$r[$p+1]];
                        if $p+(s+1) < rsz {
                            tmp += table[$v][$r[$p+(s+1)]];
                        }
                    }
                    if $p >= s-1 {
                        tmp += table[$v][$r[$p-(s-1)]];
                    }
                }
                if $p+s < rsz {
                    tmp += table[$v][$r[$p+s]];
                }
                if $p >= s {
                    tmp += table[$v][$r[$p-s]];
                }
                tmp
            }
        };
    }
    
    macro_rules! total { ($r:ident) => {
            {
                let mut tmp = 0;
                for i in 0..rsz {
                    let md = mods[i+1];
                    if md != 0 {
                        if i+1 < rsz {
                            tmp += table[$r[i]][$r[i+1]];
                            if i+(s+1) < rsz {
                                tmp += table[$r[i]][$r[i+(s+1)]];
                            }
                        }
                    }
                    if md != 1 && i+(s-1) < rsz {
                        tmp += table[$r[i]][$r[i+(s-1)]];
                    }
                    if i+s < rsz {
                        tmp += table[$r[i]][$r[i+s]];
                    }
                }
                tmp
            }
        };
    }
    
    {
        use std::cmp::Ordering;
        
        let mut dists = vec![0; rsz];
        for i in 0..rsz {
            let dy = (i / s) as i32 - s as i32;
            let dx = mods[i] as i32 - s as i32;
            dists[i] = dx * dx + dy * dy
        }
        let dists = dists;
        
        #[derive(Copy, Clone, Eq, PartialEq)]
        struct Vertex {
            edge: usize,
            pos: usize,
            dist: i32,
        }
        
        impl Ord for Vertex {
            fn cmp(&self, other: &Vertex) -> Ordering {
                match other.edge.cmp(&self.edge).reverse() {
                    Ordering::Equal => other.dist.cmp(&self.dist),
                    r => r,
                }
            }
        }
        
        impl PartialOrd for Vertex {
            fn partial_cmp(&self, other: &Vertex) -> Option<Ordering> {
                Some(self.cmp(other))
            }
        }
        
        macro_rules! ec { ($r:ident; $p:expr) => {
            {
                let mut cnt = 0;
                let md = mods[$p+1];
                if md != 1 {
                    if $p > 0 {
                        if $r[$p-1] != 0 {
                            cnt += 1;
                        }
                        if $p >= s+1 && $r[$p-(s+1)] != 0 {
                            cnt += 1;
                        }
                    }
                    if $p+(s-1) < rsz && $r[$p+(s-1)] != 0 {
                        cnt += 1;
                    }
                }
                if md != 0 {
                    if $p+1 < rsz {
                        if $r[$p+1] != 0 {
                            cnt += 1;
                        }
                        if $p+(s+1) < rsz && $r[$p+(s+1)] != 0 {
                            cnt += 1;
                        }
                    }
                    if $p >= s-1 && $r[$p-(s-1)] != 0 {
                        cnt += 1;
                    }
                }
                if $p+s < rsz && $r[$p+s] != 0 {
                    cnt += 1;
                }
                if $p >= s && $r[$p-s] != 0 {
                    cnt += 1;
                }
                cnt
           }
        }}
        
        macro_rules! push { ($heap:ident; $r:ident; $p:expr) => {
            {
                let md = mods[$p+1];
                if md != 1 {
                    if $p > 0 {
                        if $r[$p-1] == 0 {
                            let e = ec!($r; $p-1);
                            $heap.push(Vertex { edge: e, pos: $p-1, dist: dists[$p-1] });
                        }
                        if $p >= s+1 && $r[$p-(s+1)] == 0 {
                            let e = ec!($r; $p-(s+1));
                            $heap.push(Vertex { edge: e, pos: $p-(s+1), dist: dists[$p-(s+1)] });
                        }
                    }
                    if $p+(s-1) < rsz && $r[$p+(s-1)] == 0 {
                        let e = ec!($r; $p+(s-1));
                        $heap.push(Vertex { edge: e, pos: $p+(s-1), dist: dists[$p+(s-1)] });
                    }
                }
                if md != 0 {
                    if $p+1 < rsz {
                        if $r[$p+1] == 0 {
                            let e = ec!($r; $p+1);
                            $heap.push(Vertex { edge: e, pos: $p+1, dist: dists[$p+1] });
                        }
                        if $p+(s+1) < rsz && $r[$p+(s+1)] == 0 {
                            let e = ec!($r; $p+(s+1));
                            $heap.push(Vertex { edge: e, pos: $p+(s+1), dist: dists[$p+(s+1)] });
                        }
                    }
                    if $p >= s-1 && $r[$p-(s-1)] == 0 {
                        let e = ec!($r; $p-(s-1));
                        $heap.push(Vertex { edge: e, pos: $p-(s-1), dist: dists[$p-(s-1)] });
                    }
                }
                if $p+s < rsz && $r[$p+s] == 0 {
                    let e = ec!($r; $p+s);
                    $heap.push(Vertex { edge: e, pos: $p+s, dist: dists[$p+s] });
                }
                if $p >= s && $r[$p-s] == 0 {
                    let e = ec!($r; $p-s);
                    $heap.push(Vertex { edge: e, pos: $p-s, dist: dists[$p-s] });
                }
            }
        }}
        
        let mut vert: Vec<_> = (1..v+1).collect();
        
        for t in 1..v+1 {
            if edge_total[t] == 0 { continue; }
            let mut heap = std::collections::BinaryHeap::new();
            let mut tres = vec![0; rsz];
            let p = rsz / 2;
            let mut vcnt = 1;
            for i in 0..v {
                if vert[i] == t {
                    if i != 0 { vert.swap(0, i); }
                    break;
                }
            }
            tres[p] = t;        
            push!(heap; tres; p);
            
            while let Some(vtx) = heap.pop() {
                let pos = vtx.pos;
                if tres[pos] != 0 { continue; }
                let mut mxsc = 0;
                let mut vp = 0;
                for i in vcnt..v {
                    let vi = vert[i];
                    let sc = calc!(tres; pos; vi);
                    if sc >= mxsc {
                        mxsc = sc;
                        vp = i;
                    }
                }
                if vp == 0 {
                    break;
                }
                let vn = vert[vp];
                if vcnt != vp { vert.swap(vcnt, vp); }
                tres[pos] = vn;
                vcnt += 1;
                if vcnt == v {
                    break;
                }
                push!(heap; tres; pos);
            }
            
            let score = total!(tres);
            if score > max_score {
                max_score = score;
                res.clone_from(&tres);
            }
        }
    }
    
    let mut xsft: u64 = 88172645463325252;
    
    macro_rules! gen { () => {
            {
                xsft = xsft ^ (xsft << 13);
                xsft = xsft ^ (xsft >> 17);
                xsft = xsft ^ (xsft << 5);
                xsft
            }
        };
    }
    
    macro_rules! rand { ($md:expr) => (gen!() % $md) }    

    let mut cur_score = max_score;
    let mut ret = vec![0; rsz];
    ret.clone_from(&res);
    
    let alpha = 0.8_f64;
    let maxloop = 40000000;
    
    let interval = 1 << 12;
    
    for t in 0..maxloop {
        if t & interval == 0 {
            let now = SystemTime::now();
            if now.cmp(&loop_out_time) == std::cmp::Ordering::Greater {
                // println!("t={}, rate={}", t, t as f64 / maxloop as f64);
                break;
            }
        }
        let i = rand!(rsz as u64) as usize;
        let j = {
            let zr = res[i] == 0;
            let mut tmp = rand!(rsz as u64) as usize;
            while (res[tmp] == 0 && zr) || tmp == i {
                tmp = rand!(rsz as u64) as usize;
            }
            tmp
        };
        let new_score = calc!(res; i; res[j]) + calc!(res; j; res[i]);
        if new_score == 0 { continue; }
        let old_score = calc!(res; i; res[i]) + calc!(res; j; res[j]);
        let tmp_score = cur_score + new_score - old_score;
        if tmp_score >= cur_score {
            res.swap(i, j);
            cur_score = tmp_score;
            if tmp_score > max_score {
                ret.clone_from(&res);
                max_score = tmp_score;
            }
            continue;
        }
        let te = alpha.powf(t as f64 / maxloop as f64);
        let ee = ((tmp_score as f64 - cur_score as f64) / te).exp();
        let pp = gen!() as f64 / std::u64::MAX as f64;
        if pp < ee {
            res.swap(i, j);
            cur_score = tmp_score;
            continue;
        }
    }
    
    for (i, r) in ret.iter().enumerate() {
        if *r == 0 {
            continue;
        }
        println!("{} {}", r, i+1);
    }
}
